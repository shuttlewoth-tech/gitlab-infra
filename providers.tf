provider "google" {
  project = local.gcp_project
  region  = local.gcp_region
  zone    = local.gcp_zone
}

provider "auth0" {
 domain = "shuttleworth.eu.auth0.com"
}