terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.76.0"
    }
    auth0 = {
      source = "auth0/auth0"
      version = "0.33.0"
    }
  }
}