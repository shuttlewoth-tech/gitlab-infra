output "nameservers" {
  value = google_dns_managed_zone.shuttleworth_tech.name_servers
}