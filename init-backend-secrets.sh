#!/usr/bin/env zsh

check_bw_unlocked(){
    if bw unlock --check > /dev/null 2>&1
    then
      echo "Bitwarden is unlocked"
    else
      echo "Bitwarden is locked"
      exit 1
    fi
}

ACCOUNT="aurelian@shuttleworth.tech"
PROJECT="shuttleworth-core"
PROJECT_ID="25243478"
STATE_NAME="default"
GITLAB_API_KEY_ID="366e5ee5-75eb-45d3-ac32-ad82012bf86c"
CLOUD_FLARE_API_KEY_ID="94d870e9-3775-41af-81b5-aebd00ec44db"
AUTH0_CLIENT_KEY_ID="93a5d78c-208f-4498-aab8-aed3013705d3"

check_bw_unlocked

echo "Fetching GITLAB_ACCESS_TOKEN"
GITLAB_ACCESS_TOKEN=$(bw get item ${GITLAB_API_KEY_ID} | jq -r '.fields[0] .value')
export GITLAB_ACCESS_TOKEN

echo "Fetching CLOUDFLARE_API_KEY"
CLOUDFLARE_API_KEY=$(bw get item ${CLOUD_FLARE_API_KEY_ID} | jq -r '.fields[0] .value')
export CLOUDFLARE_API_KEY

echo "Fetching GOOGLE_OAUTH_ACCESS_TOKEN"
GOOGLE_OAUTH_ACCESS_TOKEN=$(gcloud auth print-access-token --account ${ACCOUNT})
export GOOGLE_OAUTH_ACCESS_TOKEN
export GOOGLE_PROJECT=${PROJECT}

echo "Fetching AUTH0_CLIENT_ID"
AUTH0_CLIENT_ID=$(bw get item ${AUTH0_CLIENT_KEY_ID} | jq '.login.username')
export AUTH0_CLIENT_ID

echo "Fetching AUTH0_CLIENT_SECRET"
AUTH0_CLIENT_SECRET=$(bw get item ${AUTH0_CLIENT_KEY_ID} | jq '.login.password')
export AUTH0_CLIENT_SECRET

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$STATE_NAME/lock" \
    -backend-config="username=aurelian-shuttleworth" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"