locals {
  mx_data = ["1 aspmx.l.google.com.", "5 alt1.aspmx.l.google.com.", "5 alt2.aspmx.l.google.com.", "10 alt3.aspmx.l.google.com.", "10 alt4.aspmx.l.google.com."]
  ns_demo = [
    "ns-cloud-c1.googledomains.com.",
    "ns-cloud-c2.googledomains.com.",
    "ns-cloud-c3.googledomains.com.",
    "ns-cloud-c4.googledomains.com.",
  ]
  ns_e42-3109 = [
    "ns-cloud-a1.googledomains.com.",
    "ns-cloud-a2.googledomains.com.",
    "ns-cloud-a3.googledomains.com.",
    "ns-cloud-a4.googledomains.com.",
  ]
  w_server = "102.132.241.16"
}

resource "google_dns_managed_zone" "shuttleworth_tech" {
  dns_name = "${local.domain}."
  name = "shuttleworth-tech"

  visibility = "public"
}

resource "google_dns_record_set" "resume" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "aurelian.${local.domain}."
  rrdatas = ["aureliansh.gitlab.io."]
  ttl = 0
  type = "CNAME"
}

resource "google_dns_record_set" "gitlab_pages" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "_gitlab-pages-verification-code.aurelian.${local.domain}."
  rrdatas = ["gitlab-pages-verification-code=33b39886257a58b69fc6427899f95107"]
  ttl = 5
  type = "TXT"
}

resource "google_dns_record_set" "mail" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "${local.domain}."
  rrdatas = local.mx_data
  ttl = 5
  type = "MX"
}

resource "google_dns_record_set" "blog_gitlab_page_alias" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "blog.${local.domain}."
  rrdatas = ["shuttlewoth-tech.gitlab.io."]
  ttl = 0
  type = "CNAME"
}

resource "google_dns_record_set" "blog_gitlab_page_code" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "_gitlab-pages-verification-code.blog.${local.domain}."
  rrdatas = ["gitlab-pages-verification-code=0bb9d215f413854d2948bb5f1e5caa24"]
  ttl = 5
  type = "TXT"
}

resource "google_dns_record_set" "e42-3109_evbox_zone" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "e42-3109.${local.domain}."
  rrdatas = local.ns_e42-3109
  ttl = 5
  type = "NS"
}

resource "google_dns_record_set" "demo_zone" {
  managed_zone = google_dns_managed_zone.shuttleworth_tech.name
  name = "demo.${local.domain}."
  rrdatas = local.ns_demo
  ttl = 5
  type = "NS"
}