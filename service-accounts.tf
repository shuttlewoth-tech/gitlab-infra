resource "google_service_account" "sre_lessons" {
  account_id   = "sre-lessons"
  display_name = "SRE Lessons"
}

resource "google_project_iam_member" "project" {
  project = local.gcp_project
  role    = "roles/editor"
  member  = "serviceAccount:${google_service_account.sre_lessons.email}"
}