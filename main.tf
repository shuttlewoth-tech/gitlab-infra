terraform {
  backend "http" {
  }
}

locals {
  is_prod = terraform.workspace == "prod"

  gcp_project = "shuttleworth-core"
  gcp_region = "europe-west1"
  gcp_zone = "${local.gcp_region}-c"

  domain = "shuttleworth.tech"
}